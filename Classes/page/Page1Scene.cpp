//
//  Page1Scene.cpp
//  RecipeBook
//
//  Created by Akihiro Matsuura on 5/22/13.
//  Copyright 2013 Syuhari, Inc. All rights reserved.
//

#include "Page1Scene.h"

USING_NS_CC;

CCScene* Page1Scene::scene()
{
    CCScene *scene = CCScene::create();
    Page1Scene *layer = Page1Scene::create();
    scene->addChild(layer);
    
    return scene;
}

bool Page1Scene::init()
{
    
    if ( !RecipeBase::init() )
    {
        return false;
    }

    CCLOG("Scene1");
    
    return true;
    
}

